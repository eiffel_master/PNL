.PHONY: all
#.SECONDARY:

ifneq ($(KERNELRELEASE),)

obj-m := projet_pnl_laniel_vu_module.o
CFLAGS_projet_pnl_laniel_vu_module.o := -DDEBUG

else

  KERNELDIR ?= ../linuxKernel/linux-4.2.3/
  PWD := $(shell pwd)

all :
	$(info obj-m : $(obj-m))
	make -C $(KERNELDIR) M=$(PWD) modules
	gcc -Wall -Wextra projet_pnl_laniel_vu_sh.c -o projet_pnl_laniel_vu_sh

clean:
	make -C $(KERNELDIR) M=$(PWD) clean
	rm projet_pnl_laniel_vu_sh

endif