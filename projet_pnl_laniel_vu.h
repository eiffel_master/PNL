#define SIZE 512
#define SYNC 0
#define ASYNC 1

/**
 * @brief used by work_kill
 */
struct kill_struct {
	int signal; /*!< the signal to send*/
	pid_t pid; /*!< the pid which will receive the signal*/
};

/**
 * @brief embeded in struct to_ioctl
 */
union data {
	char *buf; /*!< used by modinfo and wait*/
	struct kill_struct kill; /*!< used by kill*/
	unsigned long long id; /*!< used by fg*/
};

/**
 * @brief send by the user to the kernel
 */
struct to_ioctl {
	char cmd[SIZE]; /*!< the command typed by the user*/
	int async; /*!< if 0 the command is synchronous, if 1 it is asynchronous*/
	union data ioctl_data; /*!< the union used by ioctl in correlation with cmd*/
	char *to_user; /*<! contains return informations*/
};

#define LIST _IOW('F', 0, struct to_ioctl *)
#define KILL _IOW('F', 1, struct to_ioctl *)
#define MODINFO _IOWR('F', 2, struct to_ioctl *)
#define MEMINFO _IOR('F', 3, struct to_ioctl *)
#define FG _IOWR('F', 4, struct to_ioctl *)
#define WAIT _IOW('F', 5, struct to_ioctl *)
#define LSMOD _IOW('F', 6, struct to_ioctl*)
#define PS _IOW('F', 7, struct to_ioctl*)