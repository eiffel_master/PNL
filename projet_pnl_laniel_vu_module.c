#include <linux/module.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/list.h>
#include <linux/mutex.h>
#include <linux/uaccess.h>
#include <linux/slab.h>
#include <linux/mm.h>
#include <linux/sched.h>
#include <linux/swap.h>
#include <linux/workqueue.h>
#include <linux/delay.h>
#include "projet_pnl_laniel_vu.h"

#define SLEEP 1 /*if defined it will active a msleep(SLEEP_DELAY)*/
#define SLEEP_DELAY 20000

#define FG_SYNC 2 /*internal value for async*/


/**
 * TODO :
 * README
 * WAIT : la valeur de retour
*/

MODULE_DESCRIPTION("Projet PNL 2016");
MODULE_VERSION("1.0");
MODULE_LICENSE("GPL");
MODULE_AUTHOR("francis laniel : francis.laniel@etu.upmc.fr son vu tuan : @etu.upmc.fr");

static int major;
static unsigned long long task_number; /*if the user wants to create a lot of
tasks...*/

struct mutex mut_fg; /*mutex for the fg variables*/
static int nb_fg; /*actual number of launched fg*/
static int fg_end; /*actual number of finished fg*/

static bool cond; /*ioctl will wait for this cond to be true*/
DECLARE_WAIT_QUEUE_HEAD(cond_wait_queue);

/**
 * @brief the head of the task list
 */
struct task_list_head {
	struct mutex mut; /*!< a mutex to lock the list during edition*/
	struct list_head list; /*!< the list itself*/
};

struct task_list_head head_task;

/**
 * @brief for each task it exists a task_list
 */
struct task_list {
	struct work_struct work; /*!< embeded struct work, so we can get the
	encompassing struct task_list*/
	struct to_ioctl ioctl; /*!< contains data used by work_* functions*/

	unsigned long long id; /*!< unique id of the task*/
	int cmd; /*<! the command type : FG, LIST, WAIT, etc.*/

	int ret; /*!< return value of the task*/

	struct list_head list;
};

void task_list_del_and_cond_signal(struct task_list *task)
{
	mutex_lock(&head_task.mut);
	list_del(&task->list);
	mutex_unlock(&head_task.mut);

	switch (task->ioctl.async) {
	case ASYNC:
		switch (task->cmd) { /*quick if ( == LIST || == ||)*/
		case LIST:
		case MEMINFO:
		case WAIT:
		case MODINFO:
		case FG:
		case LSMOD:
		case PS:
			kfree(task->ioctl.to_user); /*this buffer is only used
			by those tasks*/
			break;
		default:
			break;
		}
		kfree(task);

		break;
	case FG_SYNC:
		mutex_lock(&mut_fg);
		nb_fg--; /*decrement the number of fg*/
		fg_end++; /*and increment the number of finished fg...*/
		mutex_unlock(&mut_fg);

		break;
	case SYNC:
		cond = true;

		break;
	default: /*should never happen*/
		task->ret = -EINVAL;

		return;
	}

	if (task->ioctl.async == FG_SYNC || task->ioctl.async == SYNC)
		wake_up(&cond_wait_queue);
}

/**
 * @brief list the task which are running
 * @param my_work work_struct* used to get the task_list encompassing
 */
void work_list(struct work_struct *my_work)
{
	struct task_list *it;
	struct task_list *task;

	char *tmp;

	task = container_of(my_work, struct task_list, work);

	/*allocate a buffer for printing in user mode*/
	task->ioctl.to_user = kmalloc(sizeof(char) * SIZE, GFP_KERNEL);

	if (!task->ioctl.to_user) {
		task->ret = -ENOMEM;
		goto end;
	}

	tmp = kmalloc_array(SIZE, sizeof(char), GFP_KERNEL);

	if (!tmp) {
		task->ret = -ENOMEM;
		goto end;
	}

	mutex_lock(&head_task.mut);
	list_for_each_entry(it, &head_task.list, list) {
		/*for each entry print informations in tmp*/
		scnprintf(tmp, SIZE, "%llu\t%s", it->id, it->ioctl.cmd);

		/*then copy it in the user buffer*/
		strncat(task->ioctl.to_user, tmp, SIZE);
	}

	mutex_unlock(&head_task.mut);

	kfree(tmp);

end:
	task_list_del_and_cond_signal(task);
}

/**
 * @brief send a signal to a pid
 * @param my_work work_struct* used to get the task_list encompassing
 */
void work_kill(struct work_struct *my_work)
{
	struct task_list *task;
	struct pid *pid;

	pid_t pid_val;

#ifdef SLEEP /*to test if asynchronous works*/
	msleep(SLEEP_DELAY);
#endif

	task = container_of(my_work, struct task_list, work); /*this line is
	present in all work_* function, so we can get the data we need*/

	pid_val = task->ioctl.ioctl_data.kill.pid; /*get pid*/

	pid = find_get_pid(pid_val); /*search for it*/

	if (!pid) {
		task->ret = -ESRCH;
		goto end;
	}

	task->ret = kill_pid(pid, task->ioctl.ioctl_data.kill.signal, 1); /*send
	the signal*/

	put_pid(pid);

end:
	task_list_del_and_cond_signal(task);
}

/**
 * @brief get information on a module
 * @param my_work work_struct* used to get the task_list encompassing
 */
void work_modinfo(struct work_struct *my_work)
{
	int i;

	struct task_list *task;
	struct module *mod;

	char *tmp;

#ifdef SLEEP
	msleep(SLEEP_DELAY);
#endif

	task = container_of(my_work, struct task_list, work);
	task->ioctl.to_user = kmalloc_array(SIZE, sizeof(char), GFP_KERNEL);

	if (!task->ioctl.to_user) {
		task->ret = -ENOMEM;
		goto end;
	}

	mod = find_module(task->ioctl.ioctl_data.buf);

	if (mod) {
		/*"copy" informations in user buffer*/
		scnprintf(task->ioctl.to_user, SIZE,
			  "name : %s\naddress : %p\n", module_name(mod), mod);

		tmp = kmalloc_array(SIZE, sizeof(char), GFP_KERNEL);

		if (!tmp) {
			task->ret = -ENOMEM;
			goto end;
		}

		if (mod->version) { /*if there is a version number then we add
			it to the user buffer*/
			scnprintf(tmp, SIZE, "version : %s\n", mod->version);
			strncat(task->ioctl.to_user, tmp, SIZE);
		}

		if (mod->args) {
			scnprintf(tmp, SIZE, "command line argument : %s\n",
				  mod->args);
			strncat(task->ioctl.to_user, tmp, SIZE);
		}

		if (mod->num_kp)
			strncat(task->ioctl.to_user,
				"possible arguments :\n", SIZE);

		mutex_lock(&mod->param_lock);
		for (i = 0; i < mod->num_kp; i++) {
			scnprintf(tmp, SIZE, "\t%s\n", mod->kp[i].name);
			strncat(task->ioctl.to_user, tmp, SIZE);
		}
		mutex_unlock(&mod->param_lock);

		task->ret = 0;

		kfree(tmp);
		/*module_put(mod);*/
	} else {
		task->ret = -ENOENT;
	}

end:
	task_list_del_and_cond_signal(task);
}

/**
 * @brief permit to put on the foreground an asynchronous task
 * @param my_work work_struct* used to get the task_list encompassing
 */
void work_fg(struct work_struct *my_work)
{
	int local_nb_fg; /*each fg has a number*/
	struct task_list *task;
	struct task_list *it;

	mutex_lock(&mut_fg);
	local_nb_fg = ++nb_fg;
	mutex_unlock(&mut_fg);

	pr_debug("local_nb_fg %d nb_fg %d end_fg %d\n", local_nb_fg, nb_fg,
		 fg_end);

	task = container_of(my_work, struct task_list, work);

	task->ioctl.to_user = kmalloc_array(SIZE, sizeof(char), GFP_KERNEL);

	if (task->ioctl.ioctl_data.id == task->id) { /*we don't permit user to
		fg the fg because it will block*/
		task->ret = -EAGAIN;
		goto dec;
	}

	mutex_lock(&head_task.mut);
	list_for_each_entry(it, &head_task.list, list) { /*search for id*/
		if (it->id == task->ioctl.ioctl_data.id) {
			it->ioctl.async = FG_SYNC; /*specific asynchronous mode
			*/

			mutex_unlock(&head_task.mut);

			/*wait until the number of finished fg is equal to the
			local number*/
			wait_event(cond_wait_queue, fg_end == local_nb_fg);

			mutex_lock(&mut_fg);
			if (nb_fg == 0) /*if there is no fg*/
				fg_end = 0; /*reset the finished fg*/
			mutex_unlock(&mut_fg);

			switch (it->cmd) {
			case LIST:
			case MEMINFO:
			case MODINFO:
			case WAIT:
			case FG:
			case LSMOD:
			case PS:
				strncpy(task->ioctl.to_user, it->ioctl.to_user,
					SIZE); /*copy task buffer in fg user
					buffer*/

				kfree(it->ioctl.to_user);

				break;
			default:
				break;
			}

			task->ret = it->ret; /*get return value then free*/
			kfree(it);

			goto end;
		}
	}

	mutex_unlock(&head_task.mut);

	task->ret = -ENOENT;

dec:
	mutex_lock(&mut_fg);
	nb_fg--;
	mutex_unlock(&mut_fg);

end:
	task_list_del_and_cond_signal(task);
}

/**
 * @brief get information on the memory usage
 * @param my_work work_struct* used to get the task_list encompassing
 */
void work_meminfo(struct work_struct *my_work)
{
	struct task_list *task;
	struct sysinfo val;

#ifdef SLEEP
	msleep(SLEEP_DELAY);
#endif

	task = container_of(my_work, struct task_list, work);

	task->ioctl.to_user = kmalloc(sizeof(char) * SIZE, GFP_KERNEL);

	if (!task->ioctl.to_user) {
		task->ret = -ENOMEM;
		goto end;
	}

	si_meminfo(&val); /*get info*/
	si_swapinfo(&val);

	scnprintf(task->ioctl.to_user, SIZE,
		"MemTotal\t%zu\nMemFree\t%zu\nMemShare\t%zu\nBuffers\t%zu\nTotalHigh\t%zu\nFreeHigh\t%zu\nMemUnit\t%d\nSwapTotal\t%zu\nSwapFree\t%zu\n",
		val.totalram, val.freeram, val.sharedram, val.bufferram,
		val.totalhigh, val.freehigh, val.mem_unit, val.totalswap,
		val.freeswap);

end:
	task_list_del_and_cond_signal(task);
}

/**
 * @brief wait for pids to finish
 * @param my_work work_struct* used to get the task_list encompassing
 */
void work_wait(struct work_struct *my_work)
{
	int ret;

	char *tmp;

	int i;

	int npids;
	int end_pid;
	long *pids;

	struct pid *pid;

	int ntasks;
	struct task_struct **tasks;

	struct task_list *task;

	ret = 0;

	npids = 0;
	pids = NULL;

	ntasks = 0;

	end_pid = 0;

	task = container_of(my_work, struct task_list, work);

	while ((tmp = strsep((char **) &task->ioctl.ioctl_data.buf, " "))
		!= NULL) { /*parse the buffer to get all pids*/
		npids++;

		/*we need more space*/
		pids = krealloc(pids, npids * sizeof(long), GFP_KERNEL);

		if (!pids) {
			ret = -ENOMEM;
			goto end;
		}

		kstrtol(tmp, 10, &pids[npids - 1]); /*atol(tmp)*/

	}

	/*array of struct_task**/
	tasks = kmalloc_array(npids, sizeof(struct task_struct *), GFP_KERNEL);

	if (!tasks) {
		ret = -ENOMEM;
		goto fpids;
	}

	for (i = 0; i < npids; i++) {
		/*get struct_pid with pid*/
		pid = find_get_pid(pids[i]);

		if (pid) {
			ntasks++;
			tasks[i] = pid_task(pid, PIDTYPE_PID);

			put_pid(pid);
		} else {
			tasks[i] = NULL;
		}
	}

	if (!ntasks) { /*pids don't exist*/
		ret = -ESRCH;
		goto ftasks;
	}

	i = 0;

	do {
		if (tasks[i] && tasks[i]->exit_state == EXIT_ZOMBIE) {
			/*if task exists and exited*/
			ret = tasks[i]->state; /*this is not the true return
			value*/
			end_pid = tasks[i]->pid;
		}

		i++;
		i %= npids;
	} while (!end_pid);

	task->ioctl.to_user = kmalloc_array(SIZE, sizeof(char), GFP_KERNEL);

	if (!task->ioctl.to_user) {
		ret = -ENOMEM;
		goto ftasks;
	}

	scnprintf(task->ioctl.to_user, SIZE, "%d", end_pid);

ftasks:
	kfree(tasks);
fpids:
	kfree(pids);
end:
	task->ret = ret;
	task_list_del_and_cond_signal(task);
}

/**
 * @brief print all charged modules
 * @param my_work work_struct* used to get the task_list encompassing
 */
void work_lsmod(struct work_struct *my_work)
{
	struct task_list *task;
	struct module *it;

#ifdef SLEEP
	msleep(SLEEP_DELAY);
#endif

	task = container_of(my_work, struct task_list, work);
	task->ioctl.to_user = kmalloc(sizeof(char) * SIZE, GFP_KERNEL);

	strncat(task->ioctl.to_user, module_name(&__this_module), SIZE);
	strncat(task->ioctl.to_user, "\n", SIZE);

	list_for_each_entry(it, &__this_module.list, list){
		strncat(task->ioctl.to_user, module_name(it), SIZE);
		strncat(task->ioctl.to_user, "\n", SIZE);
	}

	task->ret = 0;

	task_list_del_and_cond_signal(task);
}

/**
 * @brief print all tasks
 * @param my_work work_struct* used to get the task_list encompassing
 */
void work_ps(struct work_struct *my_work)
{
	struct task_list *task;
	struct task_struct *it;

	char* comm;
	char* tmp;

#ifdef SLEEP
	msleep(SLEEP_DELAY);
#endif

	task = container_of(my_work, struct task_list, work);
	task->ioctl.to_user = kmalloc(sizeof(char) * SIZE, GFP_KERNEL);

	comm = kmalloc(sizeof(it->comm), GFP_KERNEL);

	if (!comm) {
		task->ret = -ENOMEM;
		goto end;
	}

	tmp = kmalloc_array(SIZE, sizeof(char), GFP_KERNEL);

	if (!tmp) {
		task->ret = -ENOMEM;
		goto commf;
	}

	for_each_process(it){
		scnprintf(tmp, SIZE, "%d\t%s\n", it->pid, get_task_comm(comm, it));
		strncat(task->ioctl.to_user, tmp, SIZE);
	}

	task->ret = 0;

	kfree(tmp);

commf:
	kfree(comm);
end:
	task_list_del_and_cond_signal(task);
}

/**
 * @brief the function that the user will use to communicate with the module
 * @param cmd the task to create
 * @param arg will be a struct to_ioctl
 * @return it returns 0 if the operation is a success nor it will return :
 * ENOTTY if the cmd is unknown
 * ENOENT if the modinfo argument or fg argument is unknown
 * ESRCH if the pid does not exist
 * EAGAIN if the user tries to the fg the fg
 * EINVAL if the async mode is not correct, it is not supposed to happen
 */
static long projet_pnl_laniel_vu_ioctl(struct file *file, unsigned int cmd,
				       unsigned long arg){
	int ret;

	struct task_list *entry;

	ret = 0;

	switch (cmd) { /*switch instead of if ( == FG || == LIST || ...)*/
	case LIST:
	case FG:
	case KILL:
	case MEMINFO:
	case MODINFO:
	case WAIT:
	case LSMOD:
	case PS:
		/*a new entry is created and added in the task list*/
		entry = kmalloc(sizeof(struct task_list), GFP_KERNEL);

		if (!entry)
			return -ENOMEM;

		if (copy_from_user(&(entry->ioctl), (struct to_ioctl *) arg,
		       sizeof(struct to_ioctl)))
			return -EFAULT;

		entry->id = task_number++;
		entry->cmd = cmd;

		if (cmd == WAIT || cmd == MODINFO) {
			entry->ioctl.ioctl_data.buf = kmalloc(SIZE, GFP_KERNEL);

			if (!entry->ioctl.ioctl_data.buf)
				return -ENOMEM;

			if (copy_from_user(entry->ioctl.ioctl_data.buf,
			       ((struct to_ioctl *) arg)->ioctl_data.buf,
				SIZE))
				return -EFAULT;
		}

		mutex_lock(&head_task.mut);
		list_add_tail(&entry->list, &head_task.list);
		mutex_unlock(&head_task.mut);

		break;
	default:
		return -ENOTTY;
	}

	switch (cmd) {
	case LIST:
		INIT_WORK(&(entry->work), work_list);

		schedule_work(&(entry->work));

		if (!entry->ioctl.async) { /*if synchronous*/
			wait_event(cond_wait_queue, cond); /*wait*/
			cond = false;

			if (copy_to_user(((struct to_ioctl *) arg)->to_user,
					entry->ioctl.to_user, SIZE))
				ret = -EFAULT;

			kfree(entry->ioctl.to_user);
			kfree(entry);
		}

		break;
	case FG:
		INIT_WORK(&(entry->work), work_fg);

		schedule_work(&(entry->work));

		if (!entry->ioctl.async) {
			wait_event(cond_wait_queue, cond);
			cond = false;

			ret = (int) entry->ret;

			if (copy_to_user(((struct to_ioctl *) arg)->to_user,
				entry->ioctl.to_user, SIZE))
				ret = -EFAULT;

			kfree(entry->ioctl.to_user);
			kfree(entry); /*we free here because we want to get the
			return value before*/
		}

		break;
	case KILL:
		INIT_WORK(&(entry->work), work_kill);

		schedule_work(&(entry->work));

		if (!entry->ioctl.async) {
			wait_event(cond_wait_queue, cond);
			cond = false;

			ret = (int) entry->ret;

			kfree(entry);
		}

		break;
	case MODINFO:
		INIT_WORK(&(entry->work), work_modinfo);

		schedule_work(&(entry->work));

		if (!entry->ioctl.async) {
			wait_event(cond_wait_queue, cond);
			cond = false;

			ret = (int) entry->ret;

			if (copy_to_user(((struct to_ioctl *) arg)->to_user,
				entry->ioctl.to_user, SIZE))
				ret = -EFAULT;

			kfree(entry->ioctl.ioctl_data.buf);
			kfree(entry);
		}

		break;
	case MEMINFO:
		INIT_WORK(&(entry->work), work_meminfo);

		schedule_work(&(entry->work));

		if (!entry->ioctl.async) {
			wait_event(cond_wait_queue, cond);
			cond = false;

			if (copy_to_user(((struct to_ioctl *) arg)->to_user,
				entry->ioctl.to_user, SIZE))
				ret = -EFAULT;

			kfree(entry->ioctl.to_user);
			kfree(entry);
		}

		break;
	case WAIT:
		INIT_WORK(&(entry->work), work_wait);

		schedule_work(&(entry->work));

		if (!entry->ioctl.async) {
			wait_event(cond_wait_queue, cond);
			cond = false;

			if (copy_to_user(((struct to_ioctl *) arg)->to_user,
				entry->ioctl.to_user, SIZE))
				ret = -EFAULT;

			ret = (int) entry->ret;

			kfree(entry->ioctl.ioctl_data.buf);
			kfree(entry);
		}

		break;
	case LSMOD:
		INIT_WORK(&(entry->work), work_lsmod);

		schedule_work(&(entry->work));

		if (!entry->ioctl.async) {
			wait_event(cond_wait_queue, cond);
			cond = false;

			if (copy_to_user(((struct to_ioctl *) arg)->to_user,
				entry->ioctl.to_user, SIZE))
				ret = -EFAULT;

			kfree(entry->ioctl.to_user);
			kfree(entry);
		}

		break;
	case PS:
		INIT_WORK(&(entry->work), work_ps);

		schedule_work(&(entry->work));

		if (!entry->ioctl.async) {
			wait_event(cond_wait_queue, cond);
			cond = false;

			if (copy_to_user(((struct to_ioctl *) arg)->to_user,
				entry->ioctl.to_user, SIZE))
				ret = -EFAULT;

			kfree(entry->ioctl.to_user);
			kfree(entry);
		}

		break;
	default:
		ret = -ENOTTY;
	}

	return ret;
}

const struct file_operations projet_pnl_laniel_vu_fops = {
	.unlocked_ioctl = projet_pnl_laniel_vu_ioctl
};

static int __init projet_pnl_laniel_vu_init(void)
{
	pr_debug("Hello module\n");

	/*initialisation*/
	task_number = 0;
	nb_fg = 0;
	fg_end = 0;

	mutex_init(&mut_fg);
	mutex_init(&head_task.mut);

	INIT_LIST_HEAD(&head_task.list);

	major = register_chrdev(0, "projet_pnl_laniel_vu_module_dev",
				&projet_pnl_laniel_vu_fops);

	return 0;
}
module_init(projet_pnl_laniel_vu_init);


static void __exit projet_pnl_laniel_vu_exit(void)
{
	struct task_list *it;
	struct task_list *sav;

	pr_debug("Bye module\n");

	flush_scheduled_work(); /*it forces all the task to end, so we avoid
	some memory leak*/

	mutex_lock(&head_task.mut); /*we avoid memory leak*/
	list_for_each_entry_safe(it, sav, &head_task.list, list) {
		list_del(&it->list);
		kfree(it);
	}
	mutex_unlock(&head_task.mut);

	mutex_destroy(&mut_fg);
	mutex_destroy(&head_task.mut);

	unregister_chrdev(major, "projet_pnl_laniel_vu_module_dev");
}
module_exit(projet_pnl_laniel_vu_exit);