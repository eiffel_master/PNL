#include <stdlib.h>
#include <stdio.h>
#include <linux/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include "projet_pnl_laniel_vu.h"

#define NBLINES 80

int main(){
	int i;
	int ret;

	int fd;

	char *buffer;
	char* tmp;

	struct to_ioctl ioctl_struct;

	buffer = calloc(SIZE, sizeof(char));

	if((fd = open("/dev/projet_pnl_laniel_vu_module_dev", O_RDWR)) == -1){
		perror("open ");
		goto end;
	}

	do{
		printf("pnlSh> ");
		fgets(buffer, SIZE, stdin);

		strncpy(ioctl_struct.cmd, buffer, SIZE);

		ioctl_struct.async = SYNC;
		if(strstr(buffer, "&"))
			ioctl_struct.async = ASYNC;

		if(strstr(buffer, "list")){

			ioctl_struct.to_user = calloc(SIZE, sizeof(char));

			if(ioctl(fd, LIST, &ioctl_struct) == -1){
				perror("ioctl LIST ");
				continue;
			}

			if(strlen(ioctl_struct.to_user))
				fprintf(stderr, "ID\tCMD\n%s", ioctl_struct.to_user);

			free(ioctl_struct.to_user);

			continue;
		}

		if(strstr(buffer, "fg")){
			strtok(buffer, " ");

			if(!(tmp = strtok(NULL, "\n"))){
				fprintf(stderr, "La commande fg attend un argument : fg id\n");
				continue;
			}

			ioctl_struct.ioctl_data.id = atoll(tmp);

			ioctl_struct.to_user = calloc(sizeof(char), SIZE);

			if(ioctl(fd, FG, &ioctl_struct) == -1){
				perror("ioctl FG ");
				continue;
			}

			if(strlen(ioctl_struct.to_user))
				fprintf(stderr, "%s", ioctl_struct.to_user);

			free(ioctl_struct.to_user);

			continue;
		}

		if(strstr(buffer, "kill")){
			strtok(buffer, " ");

			if(!(tmp = strtok(NULL, " "))){
				fprintf(stderr, "La commande kill attend deux arguments : kill signal pid\n");
				continue;
			}

			ioctl_struct.ioctl_data.kill.signal = atoi(tmp);

			if(!(tmp = strtok(NULL, "\n"))){
				fprintf(stderr, "La commande kill attend deux arguments : kill signal pid\n");
				continue;
			}

			ioctl_struct.ioctl_data.kill.pid = atoi(tmp);

			if(ioctl(fd, KILL, &ioctl_struct) == -1){
				perror("ioctl KILL ");
				continue;
			}

			continue;
		}

		if(strstr(buffer, "wait")){
			strtok(buffer, " ");

			if(ioctl_struct.async){
				if(!(tmp = strtok(NULL, "&"))){
					fprintf(stderr, "La commande wait attend au moins un argument : wait pid [pid ...]\n");
					continue;
				}
			} else {
				if(!(tmp = strtok(NULL, "\n"))){
					fprintf(stderr, "La commande wait attend au moins un argument : wait pid [pid ...]\n");
					continue;
				}
			}

			ioctl_struct.ioctl_data.buf = calloc(sizeof(char), SIZE);
			ioctl_struct.to_user = calloc(sizeof(char), SIZE);

			strncpy(ioctl_struct.ioctl_data.buf, tmp, SIZE);

			if((ret = ioctl(fd, WAIT, &ioctl_struct)) == -1){
				perror("ioctl WAIT ");
				continue;
			}

			if(strlen(ioctl_struct.to_user))
				fprintf(stderr, "pid %s end with value %d\n", ioctl_struct.to_user, ret);

			free(ioctl_struct.to_user);
			free(ioctl_struct.ioctl_data.buf);

			continue;
		}

		if(strstr(buffer, "meminfo")){

			ioctl_struct.to_user = calloc(SIZE, sizeof(char));

			if(ioctl(fd, MEMINFO, &ioctl_struct) == -1){
				perror("ioctl MEMINFO ");
				continue;
			}

			fprintf(stderr, "%s", ioctl_struct.to_user);

			free(ioctl_struct.to_user);

			continue;
		}

		if(strstr(buffer, "modinfo")){
			strtok(buffer, " ");

			if (ioctl_struct.async) {
				if(!(tmp = strtok(NULL, " "))){
					fprintf(stderr, "La commande modinfo attend un argument : modinfo name\n");
					continue;
				}
			} else {
				if(!(tmp = strtok(NULL, "\n"))){
					fprintf(stderr, "La commande modinfo attend un argument : modinfo name\n");
					continue;
				}
			}

			ioctl_struct.ioctl_data.buf = calloc(SIZE, sizeof(char));
			ioctl_struct.to_user = calloc(SIZE, sizeof(char));

			strncpy(ioctl_struct.ioctl_data.buf, tmp, SIZE);

			if(ioctl(fd, MODINFO, &ioctl_struct) == -1){
				perror("ioctl MODINFO ");
				continue;
			}

			fprintf(stderr, "%s", ioctl_struct.to_user);

			free(ioctl_struct.ioctl_data.buf);

			continue;
		}

		if(strstr(buffer, "lsmod")){

			ioctl_struct.to_user = calloc(SIZE, sizeof(char));

			if(ioctl(fd, LSMOD, &ioctl_struct) == -1){
				perror("ioctl LSMOD ");
				continue;
			}

			if(strlen(ioctl_struct.to_user))
				fprintf(stderr, "%s", ioctl_struct.to_user);

			free(ioctl_struct.to_user);

			continue;
		}

		if(strstr(buffer, "ps")){
			ioctl_struct.to_user = calloc(SIZE, sizeof(char));

			if(ioctl(fd, PS, &ioctl_struct) == -1){
				perror("ioctl PS ");
				continue;
			}

			if(strlen(ioctl_struct.to_user))
				fprintf(stderr, "PID\tNAME\n%s", ioctl_struct.to_user);

			free(ioctl_struct.to_user);

			continue;
		}

		if(strstr(buffer, "clear")){
			for(i = 0; i < NBLINES; i++)
				puts("");

			continue;
		}

		if(!strstr(buffer, "exit"))
			fprintf(stderr, "Commande : %s inconnue\n", buffer);
	}while(!strstr(buffer, "exit"));

	close(fd);
end :
	free(buffer);
	return EXIT_SUCCESS;
}