#!/bin/sh
module=projet_pnl_laniel_vu_module
shift
/sbin/insmod ./$module.ko $* || exit 1
rm -f /dev/${module}_dev
major=$(awk "\$2==\"${module}_dev\" {print \$1}" /proc/devices)
mknod /dev/${module}_dev c $major 0
chmod 640 /dev/${module}_dev